import React, { useState, useEffect } from 'react';

import './global.css'
import './App.css'
import './Sidebar.css'
import './Main.css'



function App() {
  const [latitude, setLatitude] = useState('');
  const [longitude, setLongitude] = useState('');

  useEffect(() => {
      navigator.geolocation.getCurrentPosition(
        (position) => {
          const {latitude, longitude} = position.coords;

          setLatitude(latitude);
          setLongitude(longitude);
        },
        (err) => {
            console.log(err);
        },
        {
          timeout: 30000,
        }
      )
  }, []);


  return (
    <div id = "app">
      <aside>
        <strong>Cadastrar</strong>
        <form>
          <div className = "input-block"> 
            <label htmlFor = "github_username">Usuário do Github</label>
            <input name = "github_username" id = "github_username" required/>
          </div>

          <div className = "input-block">
            <label htmlFor = "techs">Tecnologias</label>
            <input name = "techs" id = "techs" required/>
          </div>

          <div className = "input-group">
            <div className = "input-block">
              <label htmlFor = "latitude">Latitude</label>
              <input name = "latitude" id = "latitude" required value = {latitude}/>
            </div>

            <div className = "input-block">
              <label htmlFor = "longitude">Longetude</label>
              <input name = "longitude" id = "longitude" required value = {longitude}/>
            </div>
          </div>

          <button type = "submit">Salvar</button>

        </form>
      </aside>
      <main>
        <ul>
          <li className = "dev-item">
            <header>
              <img src="https://avatars.githubusercontent.com/u/91708847?s=400&u=a516abb46251c5b3e8125b0c73fbcb5f5591144c&v=4"/>
              <div className="user-info">
                <strong>Arthur Setragni</strong>
                <span>C, React</span>
              </div>
            </header>
            <p>Tamo ai na atividade</p>
            <a href="https://github.com/arthursetragni">Acessar perfil</a>
          </li>

          <li className = "dev-item">
            <header>
              <img src="https://avatars.githubusercontent.com/u/91708847?s=400&u=a516abb46251c5b3e8125b0c73fbcb5f5591144c&v=4"/>
              <div className="user-info">
                <strong>Arthur Setragni</strong>
                <span>C, React</span>
              </div>
            </header>
            <p>Tamo ai na atividade</p>
            <a href="https://github.com/arthursetragni">Acessar perfil</a>

          </li>

          <li className = "dev-item">
            <header>
              <img src="https://avatars.githubusercontent.com/u/91708847?s=400&u=a516abb46251c5b3e8125b0c73fbcb5f5591144c&v=4"/>
              <div className="user-info">
                <strong>Arthur Setragni</strong>
                <span>C, React</span>
              </div>
            </header>
            <p>Tamo ai na atividade</p>
            <a href="https://github.com/arthursetragni">Acessar perfil</a>

          </li>
        </ul>

      </main>
    </div>
  );
}

export default App;
